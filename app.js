const express = require("express");

const app = express();
const port = process.env.PORT || 2569;

app.use(express.json());

app.get('/', (req, res) => {
    res.status(200).send({
        message: 'Ok nha!!!'
    })
})

app.listen(port, () => {
  console.log(`Server is running at port ${port}`);
});
